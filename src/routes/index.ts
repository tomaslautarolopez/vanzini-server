import { Router } from "express";
import {
  baseEstateRouter,
  baseEstatePrefixUrl,
} from "../entities/BaseEstate/routes";
import {
  baseEstatesListPrefixUrl,
  baseEstatesListRouter,
} from "../entities/BaseEstatesList/routes";
import { employeePrefixUrl, employeeRouter } from "../entities/Employee/routes";
import {
  locationFiltersPrefixUrl,
  locationFiltersRouter,
} from "../entities/LocationFilters/routes";

export const mainApiRouter = Router();

mainApiRouter.use(baseEstatePrefixUrl, baseEstateRouter);
mainApiRouter.use(locationFiltersPrefixUrl, locationFiltersRouter);
mainApiRouter.use(baseEstatesListPrefixUrl, baseEstatesListRouter);
mainApiRouter.use(employeePrefixUrl, employeeRouter);
