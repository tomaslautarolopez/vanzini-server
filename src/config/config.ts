import dotenv from "dotenv";
import path from "path";

dotenv.config({
  path: path.join(__dirname, `./${process.env.NODE_ENV}.env`),
});

export const tokkoKey = process.env.TOKKO_KEY as string;
export const cdnImageKey = process.env.CDN_IMAGE_KEY as string;
export const port = Number(process.env.PORT);
export const node_env = process.env.NODE_ENV as string;
export const db_uri = process.env.DB_URI as string;
export const db_name = process.env.DB_NAME as string;
export const openCageDataKey = process.env.OPEN_CAGE_DATA_KEY as string;
export const encryptionKey = process.env.ENCRYPTION_KEY as string;
