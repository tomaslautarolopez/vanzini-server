import { Response, Request, ErrorRequestHandler, NextFunction } from "express";
import { CustomError } from "./CustomError";

const debug = require("debug")("middlewares:errorHandlerMiddleware");

export const errorHandlerMiddleware: ErrorRequestHandler = (
  error: CustomError | Error,
  _req: Request,
  res: Response,
  _next: NextFunction
): void => {
  debug({ error });

  if (error instanceof CustomError) {
    const { status, message, errorCode } = error;
    res.status(status || 500).send({ error: message, errorCode });
  } else {
    res.status(500).send({ error: "Something went wrong" });
  }
};
