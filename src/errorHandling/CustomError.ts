import { BASE_ESTATE_ERRORS } from "../entities/BaseEstate/error";
import { BASE_ESTATES_LIST_ERRORS } from "../entities/BaseEstatesList/error";
import { EMPLOYEE_ERRORS } from "../entities/Employee/error";

export type ERROR_CODES =
  | BASE_ESTATE_ERRORS
  | BASE_ESTATES_LIST_ERRORS
  | EMPLOYEE_ERRORS;

export class CustomError extends Error {
  message: string;
  status?: number;
  errorCode?: ERROR_CODES;

  constructor(status = 500, msg: any, errorCode: ERROR_CODES) {
    super();
    this.message = msg;
    this.status = status;
    this.errorCode = errorCode;
  }
}
