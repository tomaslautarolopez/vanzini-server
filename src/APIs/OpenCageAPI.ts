import axios from "axios";
import { computeDestinationPoint } from "geolib";
import { openCageDataKey } from "../config/config";

const debug = require("debug")("apis:OpenCageAPI");

export class OpenCageAPI {
  static axiosOpenCageApiInstance = axios.create({
    baseURL: "https://api.opencagedata.com",
    params: {
      key: openCageDataKey,
    },
  });
  static OPEN_CAGE_DATA_GEODECODING: string = "/geocode/v1/json";

  async getAddressGeolocation(address: string) {
    debug("Getting address geo location");

    const {
      data: { results: openCageCoordinates },
    } = await OpenCageAPI.axiosOpenCageApiInstance.get(
      OpenCageAPI.OPEN_CAGE_DATA_GEODECODING,
      {
        params: {
          q: `${address} Rosario Santa Fe`,
          bounds: "-60.90614,-33.07567,-60.46257,-32.76191",
        },
      }
    );

    if (openCageCoordinates.length) {
      const locationCoordinates = openCageCoordinates[0].geometry;
      const rightTopCorner = computeDestinationPoint(
        {
          lat: locationCoordinates.lat,
          lon: locationCoordinates.lng,
        },
        500,
        45
      );
      const leftBottomCorner = computeDestinationPoint(
        {
          lat: locationCoordinates.lat,
          lon: locationCoordinates.lng,
        },
        500,
        225
      );

      return [
        [
          [rightTopCorner.latitude, rightTopCorner.longitude],
          [rightTopCorner.latitude, leftBottomCorner.longitude],
          [leftBottomCorner.latitude, leftBottomCorner.longitude],
          [leftBottomCorner.latitude, rightTopCorner.longitude],
          [rightTopCorner.latitude, rightTopCorner.longitude],
        ],
      ];
    }

    return [];
  }
}
