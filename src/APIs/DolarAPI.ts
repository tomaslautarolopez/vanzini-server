import axios from "axios";

const debug = require("debug")("apis:DolarAPI");

export class DolarAPI {
  static axiosDolarGaliciaApiInstance = axios.create({
    baseURL: "https://www.bancogalicia.com/cotizacion/cotizar",
  });

  static axiosDolarBalanzApiInstance = axios.create({
    baseURL: "https://www.balanz.com/api/cotizacionmoneda/2",
  });

  async getDolarGalicia(): Promise<number> {
    debug("from GALICIA")

    const { data } = await DolarAPI.axiosDolarGaliciaApiInstance.get("", {
      params: {
        currencyId: "02",
        quoteType: "SU",
        quoteId: "999",
      },
    });

    return Number(data.sell.replace(".", "").replace(",", "."));
  }

  async getDolarBalanz(): Promise<number> {
    debug("from BALANZ")

    const {
      data: {
        Cotizacion: { PrecioVenta },
      },
    } = await DolarAPI.axiosDolarBalanzApiInstance.get("");

    return Number(PrecioVenta);
  }

  async localDolar(): Promise<number> {
    return 190;
  }

  async getDolar(): Promise<number> {
    debug("Getting dolar value")

    try {
      return await this.getDolarGalicia();
    } catch (error) {
      try {
        return await this.getDolarBalanz();
      } catch (error) {
        return await this.localDolar();
      }
    }
  }
}
