import axios from "axios";
import { tokkoKey } from "../config/config";

const debug = require("debug")("apis:TokkoAPI");

export enum TOKKO_URL {
  PROPERTY = "/api/v1/property",
  DEVELOPMENTS = "/api/v1/development",
}

export class TokkoAPI {
  static PROPERTIES_LIMIT = 50;
  static DEVELOPMENTS_LIMIT = 50;

  static axiosTokkoApiInstance = axios.create({
    baseURL: "https://www.tokkobroker.com",
    params: {
      key: tokkoKey,
      lang: "es_ar",
      format: "json",
    },
  });

  async getBaseEstateRaw(id: string, url: TOKKO_URL) {
    const { data: tokkoProperty } = await TokkoAPI.axiosTokkoApiInstance.get(
      `${url}/${id}`
    );

    return tokkoProperty;
  }

  async getPropertiesAmount(): Promise<number> {
    const {
      data: {
        meta: { total_count: propertiesAmount },
      },
    } = await TokkoAPI.axiosTokkoApiInstance.get(TOKKO_URL.PROPERTY, {
      params: {
        limit: 1,
        offset: 0,
      },
    });

    return propertiesAmount;
  }

  async getDevelopmentsAmount(): Promise<number> {
    const {
      data: {
        meta: { total_count: deverlopmentsAmount },
      },
    } = await TokkoAPI.axiosTokkoApiInstance.get(TOKKO_URL.DEVELOPMENTS, {
      params: {
        limit: 1,
        offset: 0,
      },
    });

    return deverlopmentsAmount;
  }

  async getDevelopmentProperties(developmentId: string) {
    const {
      data: { objects: developmentProperties },
    } = await TokkoAPI.axiosTokkoApiInstance.get(TOKKO_URL.PROPERTY, {
      params: {
        development__id: developmentId,
        limit: 100,
      },
    });

    return developmentProperties;
  }

  async getBatchProperties(
    offset: number,
    limit: number = TokkoAPI.PROPERTIES_LIMIT
  ): Promise<{
    tokkoProperties: any[];
    propertiesAmount: number;
  }> {
    const {
      data: {
        meta: { total_count: propertiesAmount, next },
        objects: tokkoProperties,
      },
    } = await TokkoAPI.axiosTokkoApiInstance.get(TOKKO_URL.PROPERTY, {
      params: {
        limit: limit,
        offset: offset,
      },
    });

    const tokkoPropertiesWithoutDevs = tokkoProperties.filter(
      (tokkoProperty: any) => tokkoProperty.development === null
    );

    return {
      tokkoProperties: tokkoPropertiesWithoutDevs,
      propertiesAmount,
    };
  }

  async checkIfBaseEstateExistsInTokko(
    id: string,
    isDevelopment: boolean
  ): Promise<string | undefined> {
    try {
      if (isDevelopment) {
        await TokkoAPI.axiosTokkoApiInstance.get(
          `${TOKKO_URL.DEVELOPMENTS}/${id}`
        );
      } else {
        await TokkoAPI.axiosTokkoApiInstance.get(`${TOKKO_URL.PROPERTY}/${id}`);
      }

      return;
    } catch (error) {
      if (error.statusCode === 404) {
        return id;
      }
    }
  }

  async getBatchDevelopments(
    offset: number,
    limit: number = TokkoAPI.DEVELOPMENTS_LIMIT
  ): Promise<{
    tokkoDevelopments: any[];
    developmentsAmount: number;
  }> {
    const {
      data: {
        meta: { total_count: developmentsAmount },
        objects: tokkoDevelopments,
      },
    } = await TokkoAPI.axiosTokkoApiInstance.get(TOKKO_URL.DEVELOPMENTS, {
      params: {
        limit: limit,
        offset: offset,
      },
    });

    return {
      tokkoDevelopments,
      developmentsAmount,
    };
  }
}
