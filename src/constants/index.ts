export enum TAGS {
  COCHERA_OPCIONAL = "Cochera Opcional",
  COCHERA = "Cochera",
  APTO_CREDITO = "Apto crédito",
}

export const PROPERTIES_TO_SKIP: string[] = ["18901", "20930", "20933"];
