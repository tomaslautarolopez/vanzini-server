import mongoose from "mongoose";
import { Express } from "express";

import { db_uri, db_name } from "../config/config";

const debug = require("debug")("data-base");

export const initMongoConnection = async (app: Express) => {
  try {
    debug("Starting data-base connection");

    await mongoose.connect(db_uri, {
      dbName: db_name,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
      keepAlive: true,
    });

    debug("Succesful ✅ Connection to DB");

    app.emit("ready");
  } catch (error) {
    debug("❌ Something went wrong while trying to connect to DB");
    debug({ error });
  }
};
