import { Currency } from "./models/BaseEstateModel";

export interface BaseEstateFilters {
  age?: Object[];
  address?: string;
  constructionStatus?: Object;
  likesAmount?: Object;
  id?: string[];
  country?: string[];
  region?: string[];
  urbanLocation?: string[];
  firstSubDivision?: string[];
  secondSubDivision?: string[];
  operationType?: string;
  baseEstateType?: string[];
  roomAmount?: Object[];
  tags?: string[];
  fromSellPrice?: number;
  toSellPrice?: number;
  fromRentPrice?: number;
  toRentPrice?: number;
  currency?: Currency;
  customTags?: string[];
  highlight?: boolean;
}

export interface QueryFilters {
  age?: string[];
  address?: string;
  constructionStatus?: string;
  likesAmount?: string;
  id?: string[];
  country?: string[];
  region?: string[];
  urbanLocation?: string[];
  firstSubDivision?: string[];
  secondSubDivision?: string[];
  operationType?: string;
  baseEstateType?: string[];
  roomAmount?: string[];
  tags?: string[];
  fromSellPrice?: string;
  toSellPrice?: string;
  fromRentPrice?: string;
  toRentPrice?: string;
  currency?: string;
  customTags?: string[];
  highlight?: string;
  fields?: string[];
  limit?: string;
  offset?: string;
  sort?: string;
}
