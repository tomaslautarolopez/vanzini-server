import { Router } from "express";
import { update } from "./actions/update";
import { updateAll } from "./actions/updateAll";
import { getAll } from "./actions/getAll";
import { highlight } from "./actions/highlight";
import { playDown } from "./actions/playDown";
import { getHighlight } from "./actions/getHighlight";
import { setBrokerPonderation } from "./actions/setBrokerPonderation";
import { get } from "./actions/get";

export const baseEstateRouter = Router();

export const baseEstatePrefixUrl = "/base-estate";

baseEstateRouter.get("/:baseEstateId", get);
baseEstateRouter.get("/", getAll);
baseEstateRouter.get("/highlight", getHighlight);

baseEstateRouter.put("/update/all", updateAll);
baseEstateRouter.put("/update/:baseEstateId", update);
baseEstateRouter.put("/highlight/:baseEstateId", highlight);
baseEstateRouter.put(
  "/set-broker-ponderation/:baseEstateId",
  setBrokerPonderation
);

baseEstateRouter.delete("/highlight/:baseEstateId", playDown);
