import { DocumentType } from "@typegoose/typegoose";
import slugify from "slugify";
import { OpenCageAPI } from "../../APIs/OpenCageAPI";
import { TokkoAPI, TOKKO_URL } from "../../APIs/TokkoAPI";
import { PROPERTIES_TO_SKIP, TAGS } from "../../constants";
import { CustomError } from "../../errorHandling/CustomError";
import { cleanUndefinedValuesInObject } from "../../utils/cleanUndefinedValuesInObject";
import { createNumericEvaluator } from "../../utils/createNumericMongoQuery";
import { Projection } from "../../utils/createProjectionObject";
import { Sort } from "../../utils/createSortObject";
import { isAnExactAddress } from "../../utils/isAnExactAddress";
import { BASE_ESTATE_ERRORS } from "./error";
import { BaseEstateFilters, QueryFilters } from "./interfaces";
import {
  BaseEstateModel,
  BaseEstateSchema,
  Currency,
  OperationSchema,
} from "./models/BaseEstateModel";

const debug = require("debug")("entity:BaseState");

const BASE_STATE_OPERATIONS_AGGREGATE = {
  operationSellHistory: {
    $cond: {
      if: "$operationSellHistory",
      then: {
        $let: {
          vars: {
            lastOperationSellInHistory: {
              $arrayElemAt: [
                {
                  $slice: ["$operationSellHistory", -1],
                },
                0,
              ],
            },
          },
          in: {
            $cond: {
              if: {
                $eq: ["$operationSell.currencyFromTokko", "USD"],
              },
              then: {
                $cond: {
                  if: {
                    $and: [
                      {
                        $eq: [
                          "$operationSell.priceUSDmin",
                          "$$lastOperationSellInHistory.priceUSDmin",
                        ],
                      },
                      {
                        $eq: [
                          "$operationSell.priceUSDmax",
                          "$$lastOperationSellInHistory.priceUSDmax",
                        ],
                      },
                    ],
                  },
                  then: "$operationSellHistory",
                  else: {
                    $concatArrays: [
                      "$operationSellHistory",
                      ["$operationSell"],
                    ],
                  },
                },
              },
              else: {
                $cond: {
                  if: {
                    $and: [
                      {
                        $eq: [
                          "$operationSell.pricePesosMin",
                          "$$lastOperationSellInHistory.pricePesosMin",
                        ],
                      },
                      {
                        $eq: [
                          "$operationSell.pricePesosMax",
                          "$$lastOperationSellInHistory.pricePesosMax",
                        ],
                      },
                    ],
                  },
                  then: "$operationSellHistory",
                  else: {
                    $concatArrays: [
                      "$operationSellHistory",
                      ["$operationSell"],
                    ],
                  },
                },
              },
            },
          },
        },
      },
      else: ["$operationSell"],
    },
  },
};

export class BaseEstate {
  id: string;
  tokkoUrl?: TOKKO_URL;
  rawData?: any;
  chache?: DocumentType<BaseEstateSchema> | null;

  constructor(id: string) {
    this.id = id;
  }

  async get(skipCache?: boolean): Promise<BaseEstateSchema | null> {
    if (this.chache && !skipCache) {
      return this.chache;
    }

    const baseEstate = await BaseEstateModel.findById(this.id);

    if (baseEstate) {
      this.chache = baseEstate;

      return baseEstate;
    } else {
      throw new CustomError(
        404,
        "Base estate not found",
        BASE_ESTATE_ERRORS.NOT_FOUND
      );
    }
  }

  async delete() {
    const { deletedCount } = await BaseEstateModel.deleteOne({ _id: this.id });

    if (deletedCount === 0) {
      throw new CustomError(
        404,
        "Base estate not found",
        BASE_ESTATE_ERRORS.NOT_FOUND
      );
    }
  }

  async prepareForDb(_rawData: any): Promise<DocumentType<BaseEstateSchema>> {
    throw new Error("You have to implement the method prepareForDb!");
  }

  async setBrokerPonderation(brokerPonderation: number) {
    const { n: matchedCount } = await BaseEstateModel.updateOne(
      {
        _id: this.id,
      },
      {
        $set: {
          brokerPonderation,
        },
      }
    );

    if (matchedCount === 0) {
      throw new CustomError(
        404,
        "Base estate not found",
        BASE_ESTATE_ERRORS.NOT_FOUND
      );
    }
  }

  async playDown() {
    const { n: matchedCount } = await BaseEstateModel.updateOne(
      {
        _id: this.id,
      },
      {
        $set: {
          highlight: false,
          highlightTitle: null,
          highlightDescription: null,
        },
      }
    );

    if (matchedCount === 0) {
      throw new CustomError(
        404,
        "Base estate not found",
        BASE_ESTATE_ERRORS.NOT_FOUND
      );
    }
  }

  async highlight(title: string, description: string) {
    const { nModified } = await BaseEstateModel.updateOne(
      {
        _id: this.id,
      },
      {
        $set: {
          highlight: true,
          highlightTitle: title,
          highlightDescription: description,
        },
      }
    );

    if (nModified === 0) {
      throw new CustomError(
        404,
        "Base estate not found",
        BASE_ESTATE_ERRORS.NOT_FOUND
      );
    }
  }

  async update(): Promise<void> {
    if (this.tokkoUrl) {
      if (!this.rawData) {
        this.rawData = await new TokkoAPI().getBaseEstateRaw(
          this.id,
          this.tokkoUrl
        );
      }

      const baseEstate = await this.prepareForDb(this.rawData);

      await BaseEstateModel.updateOne(
        { _id: this.id },
        {
          $set: {
            ...baseEstate,
            // ...(PROPERTY_OPERATIONS_AGGREGATE as any),
          },
        },
        {
          upsert: true,
        }
      );
    }
  }

  static async generatePriceHistory() {
    debug("Starting price history aggregations");

    await BaseEstateModel.aggregate([
      {
        $set: {
          ...BASE_STATE_OPERATIONS_AGGREGATE,
        },
      },
      {
        $out: "base-estate",
      },
    ]);

    debug("Starting price history aggregations");
  }

  static operationFilterGenerator(
    operationType: string,
    fromPrice: number | undefined,
    toPrice: number | undefined,
    currency?: Currency
  ) {
    const currencyField = currency === Currency.ARS ? "pricePesos" : "priceUSD";

    return currency && (fromPrice || toPrice)
      ? {
          $and: [
            fromPrice !== undefined
              ? {
                  [`${operationType}.${currencyField}Min`]: fromPrice && {
                    $gte: fromPrice,
                  },
                }
              : undefined,
            toPrice !== undefined
              ? {
                  [`${operationType}.${currencyField}Max`]: toPrice && {
                    $lte: toPrice,
                  },
                }
              : undefined,
          ].filter((x) => !!x),
        }
      : undefined;
  }

  static async addressFilterGenerator(address: string) {
    const isExact = isAnExactAddress(address);

    if (isExact) {
      const searchBlock = await new OpenCageAPI().getAddressGeolocation(
        address
      );

      return {
        geoLocation: {
          $geoWithin: {
            $geometry: {
              type: "Polygon",
              coordinates: searchBlock,
            },
          },
        },
      };
    } else {
      return {
        $text: {
          $search: slugify(address, {
            replacement: "-",
            remove: /[*+~.()'"!:@]/g,
            lower: true,
          }),
        },
      };
    }
  }

  static customTagsFiltersGenerator(customTags?: string[]) {
    const customTagsWithOutAptCredito =
      customTags &&
      customTags.filter((customTag: string) => customTag !== TAGS.APTO_CREDITO);

    return customTagsWithOutAptCredito && customTagsWithOutAptCredito.length
      ? {
          $all: customTagsWithOutAptCredito,
        }
      : undefined;
  }

  static tagsFiltersGenerator(tags?: string[], customTags?: string[]) {
    const tagsWitouthGarage = [
      ...(tags ? tags.filter((tag) => tag !== TAGS.COCHERA_OPCIONAL) : []),
      ...(customTags && customTags.includes(TAGS.APTO_CREDITO)
        ? [TAGS.APTO_CREDITO]
        : []),
    ];

    return tagsWitouthGarage && tagsWitouthGarage.length
      ? {
          $all: [...tagsWitouthGarage],
        }
      : undefined;
  }

  static numericComparissonArrayFilterGenerator(
    field: string,
    numericComparisonArray?: object[]
  ) {
    return numericComparisonArray
      ? {
          $or: [
            ...numericComparisonArray.map((cond) => ({
              [field]: cond,
            })),
            ...numericComparisonArray.map((cond) => ({
              [`developmentProperties.${field}`]: cond,
            })),
          ],
        }
      : undefined;
  }

  static async baseEstateTypeFilterGenerator(baseEstateType?: string[]) {
    return baseEstateType
      ? {
          $or: [
            { baseEstateType: { $in: baseEstateType } },
            ...baseEstateType.map((propTy) => ({
              subPropertiesTypes: {
                $elemMatch: { $eq: propTy },
              },
            })),
          ],
        }
      : undefined;
  }

  static async filtersGenerator({
    address,
    constructionStatus,
    age,
    id,
    country,
    region,
    urbanLocation,
    firstSubDivision,
    secondSubDivision,
    operationType,
    baseEstateType,
    roomAmount,
    tags,
    currency,
    toSellPrice,
    fromSellPrice,
    fromRentPrice,
    toRentPrice,
    customTags,
    highlight,
    likesAmount,
  }: BaseEstateFilters): Promise<object> {
    // We will take into account properties of the filter type or a development that has some sub-property with that type
    const baseEstateTypeFilter =
      this.baseEstateTypeFilterGenerator(baseEstateType);

    const ageFilter = this.numericComparissonArrayFilterGenerator("age", age);

    const roomAmountFilter = this.numericComparissonArrayFilterGenerator(
      "roomAmount",
      roomAmount
    );

    const addressFilter = address
      ? await this.addressFilterGenerator(address)
      : {};

    const sellFilter = this.operationFilterGenerator(
      "operationSell",
      fromSellPrice,
      toSellPrice,
      currency
    );

    const rentFilter = this.operationFilterGenerator(
      "operationRent",
      fromRentPrice,
      toRentPrice,
      currency
    );

    const andFilters = [
      baseEstateTypeFilter,
      sellFilter,
      rentFilter,
      roomAmountFilter,
      ageFilter,
    ].filter((fil) => !!fil);

    const countryFilter = country &&
      country.length && {
        $eq: country,
      };
    const regionFilter = region &&
      region.length && {
        $eq: region,
      };
    const urbanLocationFilter = urbanLocation &&
      urbanLocation.length && {
        $eq: urbanLocation,
      };
    const firstSubDivisionFilter = firstSubDivision &&
      firstSubDivision.length && {
        $eq: firstSubDivision,
      };
    const secondSubDivisionFilter = secondSubDivision &&
      secondSubDivision.length && {
        $eq: secondSubDivision,
      };

    const customTagsFilter = this.customTagsFiltersGenerator(customTags);

    const tagsFilter = this.tagsFiltersGenerator(tags, customTags);

    const filterObject = {
      $and: andFilters.length ? andFilters : undefined,
      _id: id
        ? {
            $in: id,
          }
        : undefined,
      highlight: highlight ? { $eq: true } : undefined,
      country: countryFilter,
      region: regionFilter,
      urbanLocation: urbanLocationFilter,
      firstSubDivision: firstSubDivisionFilter,
      secondSubDivision: secondSubDivisionFilter,
      constructionStatus,
      likesAmount,
      hasGarage: tags && tags.includes("Cochera Opcional") ? true : undefined,
      sell: operationType === "Venta" ? true : undefined,
      rent: operationType === "Alquiler" ? true : undefined,
      customTags: customTagsFilter,
      tags: tagsFilter,
      ...addressFilter,
    };

    return filterObject;
  }

  static calculateBaseEstateHighlightScore(
    baseEstate: BaseEstateSchema,
    filtersAndTakenIds: QueryFilters & { alreadyTakeIds?: string[] }
  ) {
    const {
      roomAmount,
      developmentProperties,
      baseEstateType,
      tags,
      _id,
      score,
      brokerPonderation,
    } = baseEstate;

    const {
      roomAmount: roomAmountFilter,
      baseEstateType: baseEstateTypeFilter,
      tags: tagsFilter,
      customTags: customTagsFilter,
      alreadyTakeIds,
    } = filtersAndTakenIds;

    /*
         | roomAmountScore
    25% -|     
         | roomAmountDevPropScore
    
    35% - baseEstateTypeScore

         | customTagsScore
    15% -|
         | tagsScore

    25% -| locationScore

    -75% | Already seen
    */

    const tagsWitouthGarage = [
      ...(tagsFilter
        ? tagsFilter.filter((tag) => tag !== TAGS.COCHERA_OPCIONAL)
        : []),
      ...(customTagsFilter && customTagsFilter.includes(TAGS.APTO_CREDITO)
        ? [TAGS.APTO_CREDITO]
        : []),
    ];
    const customTagsWithOutAptCredito =
      customTagsFilter &&
      customTagsFilter.filter(
        (customTag: string) => customTag !== TAGS.APTO_CREDITO
      );

    const roomAmountEvaluators =
      roomAmountFilter && roomAmountFilter.map(createNumericEvaluator);
    const roomAmountScore =
      roomAmount && roomAmountEvaluators
        ? roomAmountEvaluators.reduce(
            (accumulator, currentValue) =>
              accumulator || currentValue(roomAmount),
            false
          )
          ? 1
          : 0
        : 0;
    const roomAmountDevPropScore =
      roomAmountEvaluators && developmentProperties
        ? developmentProperties
            .map(
              (devProp: BaseEstateSchema | undefined | string) =>
                typeof devProp !== "string" && devProp?.roomAmount
            )
            .map((rma) =>
              roomAmountEvaluators.reduce(
                (accumulator, currentValue) =>
                  accumulator || (!!rma && currentValue(rma)),
                false
              )
            )
            .some((a) => a)
          ? 1
          : 0
        : 0;
    const roomPercentages = Math.max(
      roomAmountScore * 25,
      roomAmountDevPropScore * 25
    );

    // Property type score
    const baseEstateTypePercentage =
      baseEstateTypeFilter &&
      baseEstateTypeFilter.find((propType) => propType === baseEstateType)
        ? 35
        : 0;

    // Tags scores
    const tagsScore =
      tagsWitouthGarage && tagsWitouthGarage.length
        ? tagsWitouthGarage.filter((tag) => !!tags && tags.includes(tag)).length
        : 0;
    const customTagsScore =
      customTagsWithOutAptCredito && customTagsWithOutAptCredito.length
        ? customTagsWithOutAptCredito.filter(
            (tag) => !!tags && tags.includes(tag)
          ).length
        : 0;

    const tagsPercentage =
      (tagsScore + customTagsScore) /
      ((tags ? tags.length : 0) + (tags ? tags.length : 0));

    // Tags scores
    const newPercentage = alreadyTakeIds
      ? alreadyTakeIds.includes(_id)
        ? -75
        : 0
      : 0;

    const totalScore =
      roomPercentages +
      baseEstateTypePercentage +
      tagsPercentage +
      (brokerPonderation || 0) +
      newPercentage +
      (score ? score * 25 : 0);

    return totalScore;
  }

  static async getHighlight(
    filters: QueryFilters & { alreadyTakeIds?: string[] }
  ): Promise<BaseEstateSchema | undefined> {
    const { address } = filters;

    const highlightFilter = address
      ? {
          $or: [
            {
              _id: { $exists: true },
            },
            {
              $text: {
                $search: slugify(address, {
                  replacement: "-",
                  remove: /[*+~.()'"!:@]/g,
                  lower: true,
                }),
              },
            },
          ],
        }
      : {};

    const projection: Projection = {};

    if (filters.address) {
      projection.score = { $meta: "textScore" };
    }

    const highlightedBaseEstates = await BaseEstateModel.find(
      {
        highlight: { $eq: true },
        ...highlightFilter,
      },
      projection
    ).map((baseEstates) => {
      return baseEstates.map((baseEstate) => {
        baseEstate.score = this.calculateBaseEstateHighlightScore(
          baseEstate,
          filters
        );
        return baseEstate;
      });
    });

    return highlightedBaseEstates.sort(
      (fsProp: BaseEstateSchema, scProp: BaseEstateSchema) =>
        (scProp.score || 0) - (fsProp.score || 0)
    )[0];
  }

  static async getAll(
    filters: BaseEstateFilters,
    sort: Sort,
    offset: number,
    projection: Projection | undefined = {},
    limit: number | undefined
  ): Promise<{
    baseEstates: BaseEstateSchema[];
    totalCount: number;
    realCount: number;
  }> {
    const dbfilters = cleanUndefinedValuesInObject(
      await this.filtersGenerator(filters)
    );

    const dbOptions = {
      skip: offset,
      limit: limit,
    };

    if (filters.address && !isAnExactAddress(filters.address)) {
      projection.score = { $meta: "textScore" };
      sort.score = { $meta: "textScore" };
    }

    const [baseEstates, realCount, totalCount] = await Promise.all([
      BaseEstateModel.find(dbfilters, projection, dbOptions).sort({
        ...sort,
      }),
      BaseEstateModel.countDocuments(dbfilters),
      BaseEstateModel.aggregate([
        {
          $match: dbfilters,
        },
        {
          $unwind: {
            path: "$developmentProperties",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $count: "propertiesCount",
        },
      ]),
    ]);

    return {
      baseEstates,
      totalCount: totalCount.length && totalCount[0].propertiesCount,
      realCount,
    };
  }

  static async baseEstatesChecker() {
    const baseEstates = await BaseEstateModel.find(
      { _id: { $nin: PROPERTIES_TO_SKIP } },
      { _id: 1, baseEstateType: 1 }
    );
    const tokkoApi = new TokkoAPI();

    const baseEstatesToDelete = (
      await Promise.all(
        baseEstates.map(({ _id, baseEstateType }) =>
          tokkoApi.checkIfBaseEstateExistsInTokko(
            _id,
            !!baseEstateType && this.isDevelopment(baseEstateType)
          )
        )
      )
    ).filter((baseEstateId) => !!baseEstateId);

    await Promise.all(
      (baseEstatesToDelete as string[]).map((baseEstateId) =>
        new BaseEstate(baseEstateId).delete()
      )
    );
  }

  static isDevelopment(baseEstateType: string): boolean {
    return baseEstateType === "BU" || baseEstateType === "LOT";
  }

  static rentPriceGenerator(
    operations: any,
    usdExchange: number
  ): OperationSchema {
    const rentOperations = operations.find(
      (opt: any) => opt.operation_type === "Alquiler"
    );
    const usdRent =
      rentOperations &&
      rentOperations.prices &&
      rentOperations.prices.find((opt: any) => opt.currency === Currency.USD);
    const pesosRent =
      rentOperations &&
      rentOperations.prices &&
      rentOperations.prices.find((opt: any) => opt.currency === Currency.ARS);
    const originalCurrency =
      rentOperations &&
      rentOperations.prices &&
      rentOperations.prices.find((op: any) => op.currency === Currency.USD)
        ? Currency.USD
        : Currency.ARS;

    return {
      priceUSDmin: usdRent
        ? usdRent.price
        : pesosRent && Math.ceil(pesosRent.price / usdExchange),
      priceUSDmax: usdRent
        ? usdRent.price
        : pesosRent && Math.ceil(pesosRent.price / usdExchange),
      pricePesosMin: pesosRent
        ? pesosRent.price
        : usdRent && Math.ceil(usdRent.price * usdExchange),
      pricePesosMax: pesosRent
        ? pesosRent.price
        : usdRent && Math.ceil(usdRent.price * usdExchange),
      currencyFromTokko: originalCurrency,
      date: new Date().getTime(),
    };
  }

  static sellPriceGenerator(
    operations: any,
    usdExchange: number
  ): OperationSchema {
    const sellOperations = operations.find(
      (opt: any) => opt.operation_type === "Venta"
    );
    const usdSell =
      sellOperations &&
      sellOperations.prices &&
      sellOperations.prices.find((opt: any) => opt.currency === Currency.USD);
    const pesosSell =
      sellOperations &&
      sellOperations.prices &&
      sellOperations.prices.find((opt: any) => opt.currency === Currency.ARS);

    const originalCurrency =
      sellOperations &&
      sellOperations.prices &&
      sellOperations.prices.find((opt: any) => opt.currency === Currency.USD)
        ? Currency.USD
        : Currency.ARS;

    return {
      priceUSDmin: usdSell
        ? usdSell.price
        : pesosSell && Math.ceil(pesosSell.price / usdExchange),
      priceUSDmax: usdSell
        ? usdSell.price
        : pesosSell && Math.ceil(pesosSell.price / usdExchange),
      pricePesosMin: pesosSell
        ? pesosSell.price
        : usdSell && Math.ceil(usdSell.price * usdExchange),
      pricePesosMax: pesosSell
        ? pesosSell.price
        : usdSell && Math.ceil(usdSell.price * usdExchange),
      currencyFromTokko: originalCurrency,
      date: new Date().getTime(),
    };
  }
}
