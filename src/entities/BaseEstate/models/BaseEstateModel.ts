import {
  prop,
  getModelForClass,
  modelOptions,
  index,
  Ref,
} from "@typegoose/typegoose";
import * as mongoose from "mongoose";

export enum Currency {
  USD = "USD",
  ARS = "ARS",
}

export class GeoLocationSchema {
  @prop()
  public type: string;

  @prop({ type: Number })
  public coordinates: number[];
}

export class TagSchema {
  @prop()
  public id: number;

  @prop()
  public name: string;

  @prop()
  public type: number;
}

export class ProducerSchema {
  @prop()
  cellphone: string;

  @prop()
  email: string;

  @prop()
  id: number;

  @prop()
  name: string;

  @prop()
  phone: string;

  @prop()
  picture: string;
}

export class CustomTagsInfoSchema {
  @prop()
  id: number;

  @prop()
  group_name: string;

  @prop()
  name: string;

  @prop()
  public_name: string;
}

export class OperationSchema {
  @prop()
  priceUSDmin: number;

  @prop()
  priceUSDmax: number;

  @prop()
  pricePesosMin: number;

  @prop()
  pricePesosMax: number;

  @prop({ enum: Currency, type: String })
  currencyFromTokko: Currency;

  @prop()
  date: number;
}

export class PhotoSchema {
  @prop()
  description: string;

  @prop()
  image: string;

  @prop()
  is_blueprint: boolean;

  @prop()
  is_front_cover: boolean;

  @prop()
  order: number;

  @prop()
  original: string;

  @prop()
  thumb: string;
}

@index({ addressSlug: "text" })
// @index({ geoLocation: "2d" })
@modelOptions({ schemaOptions: { collection: "base-estate" } })
export class BaseEstateSchema {
  @prop()
  public _id: string;

  @prop()
  public zonification?: string;

  @prop()
  public brokerPonderation?: number;

  @prop()
  public country?: string;

  @prop()
  public region?: string;

  @prop()
  public firstSubDivision?: string;

  @prop()
  public secondSubDivision?: string;

  @prop()
  public roomAmount?: number;

  @prop()
  public bathroomAmount?: number;

  @prop()
  public toiletAmount?: number;

  @prop()
  public garageAmount?: number;

  @prop()
  public publicationTitle?: string;

  @prop()
  public situation?: string;

  @prop()
  public disposition?: string;

  @prop()
  public expenses?: number;

  @prop()
  public age: number;

  @prop()
  public constructionStatus?: number;

  @prop()
  public webPrice?: boolean;

  @prop()
  public hasGarage?: boolean;

  @prop({ _id: false, type: () => GeoLocationSchema })
  public geoLocation?: GeoLocationSchema;

  @prop()
  public sell?: boolean;

  @prop()
  public rent?: boolean;

  @prop()
  public creationDate?: number;

  @prop()
  public urbanLocation?: string;

  @prop()
  public constructionDate?: string;

  @prop()
  public unroofedSurface?: number;

  @prop()
  public roofedSurface?: number;

  @prop()
  public semiroofedSurface?: number;

  @prop()
  public totalSurfaceMin: number;

  @prop()
  public totalSurfaceMax: number;

  @prop()
  public surface?: number;

  @prop()
  public description?: string;

  @prop({ type: String })
  public tags?: string[];

  @prop({ type: String })
  public customTags?: string[];

  @prop()
  public addressSlug?: string;

  @prop()
  public address?: string;

  @prop()
  public image360?: string;

  @prop()
  public propertyCondition?: string;

  @prop()
  public orientation?: string;

  @prop()
  public ambientAmount?: number;

  @prop()
  public floorsAmount?: number;

  @prop({ _id: false, type: () => ProducerSchema })
  public producer?: ProducerSchema;

  @prop({ _id: false, type: () => [TagSchema] })
  public tagsInfo?: TagSchema[];

  @prop({ _id: false, type: () => [CustomTagsInfoSchema] })
  customTagsInfo?: CustomTagsInfoSchema[];

  @prop({ _id: false, type: () => OperationSchema })
  operationSell: OperationSchema;

  @prop({ _id: false, type: () => OperationSchema })
  operationRent: OperationSchema;

  @prop({ _id: false, type: () => [PhotoSchema] })
  photos?: PhotoSchema[];

  @prop({ ref: () => BaseEstateSchema })
  public developmentProperties?: Ref<BaseEstateSchema>[];

  @prop({ ref: () => BaseEstateSchema })
  public development?: Ref<BaseEstateSchema>;

  @prop({ _id: false, type: () => [OperationSchema] })
  operationSellHistory?: OperationSchema[];

  @prop({ _id: false, type: () => OperationSchema })
  lastOperarionSell?: OperationSchema;

  @prop()
  baseEstateType?: string;

  @prop({ type: String })
  subPropertiesTypes?: string[];

  @prop()
  score?: number;

  @prop()
  highlight?: boolean;

  @prop()
  highlightTitle?: string | null;

  @prop()
  highlightDescription?: string | null;
}

export const BaseEstateModel = getModelForClass(BaseEstateSchema);
