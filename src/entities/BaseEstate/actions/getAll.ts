import { RequestHandler } from "express";
import { createNumericMongoQuery } from "../../../utils/createNumericMongoQuery";
import { createProjectionObject } from "../../../utils/createProjectionObject";
import { createSortObject } from "../../../utils/createSortObject";
import { BaseEstate } from "../BaseEstate";
import { BaseEstateFilters, QueryFilters } from "../interfaces";
import { Currency } from "../models/BaseEstateModel";

const debug = require("debug")("actions:BaseEstate:getAll");

export const getAll: RequestHandler<any, any, any, QueryFilters> = async (
  req,
  res,
  next
) => {
  const {
    address,
    constructionStatus,
    fields,
    id,
    limit: limitString,
    offset: offsetString,
    operationType,
    baseEstateType,
    roomAmount,
    tags,
    fromSellPrice,
    toSellPrice,
    fromRentPrice,
    customTags,
    toRentPrice,
    currency,
    sort: sortString,
    age,
    highlight,
    country,
    region,
    urbanLocation,
    firstSubDivision,
    likesAmount,
    secondSubDivision,
  } = req.query;

  const filter: BaseEstateFilters = {
    address,
    constructionStatus:
      constructionStatus && createNumericMongoQuery(constructionStatus),
    id,
    age: age && age.map(createNumericMongoQuery),
    likesAmount: likesAmount && createNumericMongoQuery(likesAmount),
    operationType,
    baseEstateType,
    fromSellPrice:
      currency && fromSellPrice ? Number(fromSellPrice) : undefined,
    toSellPrice: currency && toSellPrice ? Number(toSellPrice) : undefined,
    fromRentPrice:
      currency && fromRentPrice ? Number(fromRentPrice) : undefined,
    toRentPrice: currency && toRentPrice ? Number(toRentPrice) : undefined,
    currency: currency as Currency,
    roomAmount: roomAmount && roomAmount.map(createNumericMongoQuery),
    tags,
    customTags,
    highlight: !!highlight,
    country,
    region,
    urbanLocation,
    firstSubDivision,
    secondSubDivision,
  };

  const projection = fields && createProjectionObject(fields);
  const limit = limitString ? Number(limitString) : undefined;
  const offset = offsetString ? Number(offsetString) : 0;

  const sort = sortString ? createSortObject(sortString) : { creationDate: -1 };

  try {
    debug("Calling action");

    const { baseEstates, totalCount, realCount } = await BaseEstate.getAll(
      filter,
      sort,
      offset,
      projection,
      limit
    );

    debug("Succesful ✅");
    debug({ totalCount, realCount });

    res.status(200);
    res.send({
      baseEstates,
      totalCount,
      realCount,
    });
  } catch (error) {
    debug("❌ Something went wrong");

    next(error);
  }
};
