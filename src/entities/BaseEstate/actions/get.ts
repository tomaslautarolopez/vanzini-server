import { RequestHandler } from "express";
import { BaseEstate } from "../BaseEstate";

const debug = require("debug")("actions:BaseEstate:get");

export const get: RequestHandler = async (req, res, next) => {
  const { baseEstateId } = req.params;

  try {
    debug("Calling action");

    const baseEstate = await new BaseEstate(baseEstateId).get();

    debug(`Succesful ✅ ${baseEstateId}`);

    res.status(200);
    res.send(baseEstate);
  } catch (error) {
    debug(`❌ Something went wrong ${baseEstateId}`);

    next(error);
  }
};
