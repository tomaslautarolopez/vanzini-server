import { RequestHandler } from "express";
import { DolarAPI } from "../../../APIs/DolarAPI";
import { Development } from "../../Development/Development";
import { Property } from "../../Property/Property";

const debug = require("debug")("actions:BaseEstate:update");

export const update: RequestHandler = async (req, res, next) => {
  const { baseEstateId } = req.params;
  const { isDevelopment } = req.body;

  try {
    debug("Calling action");

    const dolarValue = await new DolarAPI().getDolar();

    if (isDevelopment) {
      await new Development(baseEstateId, dolarValue).update();
    } else {
      await new Property(baseEstateId, dolarValue).update();
    }

    debug(`Succesful ✅ ${baseEstateId}`);

    res.status(200);
    res.send("Successful update");
  } catch (error) {
    debug(`❌ Something went wrong ${baseEstateId}`);

    next(error);
  }
};
