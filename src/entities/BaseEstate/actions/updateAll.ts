import { RequestHandler } from "express";
import { Developments } from "../../Development/Developments";
import { LocationsFilters } from "../../LocationFilters/LocationsFilters";
import { Properties } from "../../Property/Properties";
import { BaseEstate } from "../BaseEstate";

const debug = require("debug")("actions:BaseEstate:updateAll");

export const updateAllAction = async () => {
  const properties = new Properties();
  const developments = new Developments();
  const locationFilters = new LocationsFilters();

  await Promise.all([properties.update(), developments.update()]);
  await BaseEstate.generatePriceHistory();

  locationFilters.generate();
  BaseEstate.baseEstatesChecker();
};

export const updateAll: RequestHandler = async (req, res, next) => {
  try {
    debug("Calling action");

    await updateAllAction();

    debug("Succesful ✅");

    res.status(200);
    res.send("Successful update");
  } catch (error) {
    debug("❌ Something went wrong");

    next(error);
  }
};
