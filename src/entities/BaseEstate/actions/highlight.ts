import { RequestHandler } from "express";
import { BaseEstate } from "../BaseEstate";

const debug = require("debug")("actions:BaseEstate:highlight");

export const highlight: RequestHandler = async (req, res, next) => {
  const { baseEstateId } = req.params;
  const { title, description } = req.body;

  try {
    debug("Calling action");

    await new BaseEstate(baseEstateId).highlight(title, description);

    debug(`Succesful ✅ highlight ${baseEstateId}`);

    res.status(200);
    res.send("Successful highlight");
  } catch (error) {
    debug(`❌ Something went wrong ${baseEstateId}`);

    next(error);
  }
};
