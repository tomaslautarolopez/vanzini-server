import { RequestHandler } from "express";
import { BaseEstate } from "../BaseEstate";

const debug = require("debug")("actions:BaseEstate:setBrokerPonderation");

export const setBrokerPonderation: RequestHandler = async (req, res, next) => {
  const { baseEstateId } = req.params;
  const { ponderation } = req.body;

  try {
    debug("Calling action");

    await new BaseEstate(baseEstateId).setBrokerPonderation(
      Number(ponderation)
    );

    debug(`Succesful ✅ set broking ponderation ${baseEstateId}`);

    res.status(200);
    res.send("Successful highlight");
  } catch (error) {
    debug(`❌ Something went wrong ${baseEstateId}`);

    next(error);
  }
};
