import { RequestHandler } from "express";
import { BaseEstate } from "../BaseEstate";
import { QueryFilters } from "../interfaces";

const debug = require("debug")("actions:BaseEstate:getHighlight");

export const getHighlight: RequestHandler<
  any,
  any,
  any,
  QueryFilters & { alreadyTakeIds?: string[] }
> = async (req, res, next) => {
  try {
    debug("Calling action");

    const highlightBaseEstate = await BaseEstate.getHighlight(req.query);
    
    debug(`Succesful ✅ get highlight`);

    res.status(200);
    res.send(highlightBaseEstate);
  } catch (error) {
    debug(`❌ Something went wrong`);

    next(error);
  }
};
