import { RequestHandler } from "express";
import { BaseEstate } from "../BaseEstate";

const debug = require("debug")("actions:BaseEstate:playDown");

export const playDown: RequestHandler = async (req, res, next) => {
  const { baseEstateId } = req.params;

  try {
    debug("Calling action");

    await new BaseEstate(baseEstateId).playDown();

    debug(`Succesful ✅ play down ${baseEstateId}`);

    res.status(200);
    res.send("Successful play down");
  } catch (error) {
    debug(`❌ Something went wrong ${baseEstateId}`);

    next(error);
  }
};
