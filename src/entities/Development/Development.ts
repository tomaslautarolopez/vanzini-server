import { cdnImageKey } from "../../config/config";
import { BaseEstate } from "../BaseEstate/BaseEstate";
import { TokkoAPI, TOKKO_URL } from "../../APIs/TokkoAPI";
import { createCustomTag } from "../../utils/createCustomTag";
import { slugAndEncode } from "../../utils/slugAndEncode";
import { hasTags } from "../../utils/hasTags";
import { TAGS } from "../../constants";
import {
  BaseEstateSchema,
  OperationSchema,
  BaseEstateModel,
  Currency,
} from "../BaseEstate/models/BaseEstateModel";
import { Property } from "../Property/Property";
import { DocumentType } from "@typegoose/typegoose";

export class Development extends BaseEstate {
  usdExchange: number;

  constructor(id: string, usdExchange: number, rawData?: any) {
    super(id);
    this.tokkoUrl = TOKKO_URL.DEVELOPMENTS;
    this.rawData = rawData;
    this.usdExchange = usdExchange;
  }

  async developmentPropertiesAndPriceGenerator(
    developmentId: string,
    usdExchange: number
  ): Promise<{
    developmentProperties: BaseEstateSchema[];
    operations: {
      operationSell: OperationSchema;
      operationRent: OperationSchema;
    };
  }> {
    const developmentPropertiesRaw =
      await new TokkoAPI().getDevelopmentProperties(developmentId);
    const developmentProperties: BaseEstateSchema[] = await Promise.all(
      developmentPropertiesRaw.map((devProp: any) =>
        this.createPropertyForDevelopmentFromTokkoStructure(
          devProp,
          usdExchange
        )
      )
    );

    const operationSell: OperationSchema = developmentProperties.reduce(
      (acum: OperationSchema, devProp: BaseEstateSchema): OperationSchema => ({
        pricePesosMax:
          Math.max(acum.pricePesosMax, devProp.operationSell.pricePesosMax) ||
          Number.MIN_SAFE_INTEGER,
        pricePesosMin:
          Math.min(acum.pricePesosMin, devProp.operationSell.pricePesosMin) ||
          Number.MAX_SAFE_INTEGER,
        priceUSDmax:
          Math.max(acum.priceUSDmax, devProp.operationSell.priceUSDmax) ||
          Number.MIN_SAFE_INTEGER,
        priceUSDmin:
          Math.min(acum.priceUSDmin, devProp.operationSell.priceUSDmin) ||
          Number.MAX_SAFE_INTEGER,
        currencyFromTokko: devProp.operationSell.currencyFromTokko,
        date: new Date().getTime(),
      }),
      {
        pricePesosMax: Number.MIN_SAFE_INTEGER,
        pricePesosMin: Number.MAX_SAFE_INTEGER,
        priceUSDmax: Number.MIN_SAFE_INTEGER,
        priceUSDmin: Number.MAX_SAFE_INTEGER,
        currencyFromTokko: Currency.USD,
        date: 0,
      }
    );

    const operationRent: OperationSchema = developmentProperties.reduce(
      (acum: OperationSchema, devProp: BaseEstateSchema): OperationSchema => ({
        pricePesosMax:
          Math.max(acum.pricePesosMax, devProp.operationRent.pricePesosMax) ||
          Number.MIN_SAFE_INTEGER,
        pricePesosMin:
          Math.min(acum.pricePesosMin, devProp.operationRent.pricePesosMin) ||
          Number.MAX_SAFE_INTEGER,
        priceUSDmax:
          Math.max(acum.priceUSDmax, devProp.operationRent.priceUSDmax) ||
          Number.MIN_SAFE_INTEGER,
        priceUSDmin:
          Math.min(acum.priceUSDmin, devProp.operationRent.priceUSDmin) ||
          Number.MAX_SAFE_INTEGER,
        currencyFromTokko: devProp.operationRent.currencyFromTokko,
        date: new Date().getTime(),
      }),
      {
        pricePesosMax: Number.MIN_SAFE_INTEGER,
        pricePesosMin: Number.MAX_SAFE_INTEGER,
        priceUSDmax: Number.MIN_SAFE_INTEGER,
        priceUSDmin: Number.MAX_SAFE_INTEGER,
        currencyFromTokko: Currency.USD,
        date: 0,
      }
    );

    return {
      developmentProperties,
      operations: {
        operationSell,
        operationRent,
      },
    };
  }

  async createPropertyForDevelopmentFromTokkoStructure(
    property: any,
    usdExchange: number
  ): Promise<DocumentType<BaseEstateSchema>> {
    const developmentProperty = await new Property(
      property.id.toString(),
      usdExchange
    ).prepareForDb(property);

    return developmentProperty;
  }

  async prepareForDb(rawData: any) {
    const { developmentProperties, operations } =
      await this.developmentPropertiesAndPriceGenerator(
        rawData.id,
        this.usdExchange
      );

    const location = rawData.location;
    const splitedLocation = location.full_location
      .split(" | ")
      .map((entry: string) => entry.trim().replace(".", " "));
    const isLoteo =
      rawData.custom_tags &&
      rawData.custom_tags.map((tag: any) => tag.name).includes("Loteo");
    const photos =
      rawData.photos &&
      rawData.photos.map((photo: any) => ({
        ...photo,
        image: `${cdnImageKey}${photo.image.split("/").slice(-2).join("/")}`,
      }));

    const totalSurfaceArray =
      developmentProperties.length &&
      developmentProperties.map((prop) => prop.totalSurfaceMax);

    const rent = developmentProperties.some((devProp) => devProp.rent);
    const sell = developmentProperties.some((devProp) => devProp.sell);
    const zonification =
      developmentProperties[0] && developmentProperties[0].zonification;

    const geoLocation = {
      type: "Point",
      coordinates: [parseFloat(rawData.geo_lat), parseFloat(rawData.geo_long)],
    };

    return new BaseEstateModel({
      _id: rawData.id.toString(),
      publicationTitle: rawData.publication_title,
      geoLocation,
      age:
        developmentProperties.length &&
        Math.max(...developmentProperties.map((prop) => prop.age)),
      location,
      country: splitedLocation[0],
      firstSubDivision:
        splitedLocation.length < 3
          ? splitedLocation[1]
          : splitedLocation[splitedLocation.length - 2],
      secondSubDivision:
        splitedLocation.length < 3
          ? splitedLocation[2]
          : splitedLocation[splitedLocation.length - 1],
      creationDate: developmentProperties.length
        ? developmentProperties[0].creationDate
        : undefined,
      producer: developmentProperties.length
        ? developmentProperties[0].producer
        : undefined,
      description: rawData.description,
      tags: rawData.tags.map((tag: any) => tag.name),
      tagsInfo: rawData.tags,
      customTags: createCustomTag(rawData.custom_tags),
      customTagsInfo: rawData.custom_tags,
      sell,
      rent,
      totalSurfaceMax: totalSurfaceArray && Math.max(...totalSurfaceArray),
      totalSurfaceMin: totalSurfaceArray && Math.min(...totalSurfaceArray),
      webPrice: rawData.web_price,
      address: rawData.fake_address,
      addressSlug: slugAndEncode(rawData.fake_address),
      baseEstateType: isLoteo ? "LOT" : "BU",
      zonification,
      garageAmount: rawData.parking_lot_amount,
      photos,
      image360: rawData.videos.length > 0 ? rawData.videos[0].url : undefined,
      constructionDate: rawData.construction_date
        ? rawData.construction_date
        : "",
      constructionStatus: rawData.construction_status,
      hasGarage:
        rawData.parking_lot_amount > 0 ||
        hasTags(rawData.tags, [TAGS.COCHERA, TAGS.COCHERA_OPCIONAL]) ||
        !!developmentProperties.find((prop) => prop.baseEstateType === "GA"),
      subPropertiesTypes: developmentProperties.map(
        (prop) => prop.baseEstateType
      ),
      developmentProperties,
      ...operations,
    });
  }
}
