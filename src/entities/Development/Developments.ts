import { DolarAPI } from "../../APIs/DolarAPI";
import { TokkoAPI } from "../../APIs/TokkoAPI";
import { EmployeeSocket } from "../Employee/socket";
import { Development } from "./Development";

const debug = require("debug")("entity:Developments");

export class Developments {
  isUpdating: boolean;
  progress: number;
  private static instance: Developments | null;
  private tokkoApi: TokkoAPI;

  constructor() {
    if (!!Developments.instance) {
      return Developments.instance;
    }

    Developments.instance = this;
    this.isUpdating = false;
    this.progress = 0;
    this.tokkoApi = new TokkoAPI();

    return this;
  }

  private async updateDevelopmentsBatch(offset: number, dolarValue: number) {
    const {
      tokkoDevelopments,
      developmentsAmount,
    } = await this.tokkoApi.getBatchDevelopments(offset);
    const employeeSocket = new EmployeeSocket();

    await Promise.all(
      tokkoDevelopments.map((tokkoProperty: any) =>
        new Development(
          tokkoProperty.id.toString(),
          dolarValue,
          tokkoProperty
        ).update()
      )
    );

    this.progress = Math.min(
      this.progress + (TokkoAPI.DEVELOPMENTS_LIMIT / developmentsAmount) * 100,
      100
    );
    employeeSocket.sendMessageToEmployeeRoom(
      "developments-progress",
      this.progress
    );

    debug("Finish updating data-base developments batch");
    debug(`Offset: ${offset}`);
    debug(`Progress: ${this.progress}`);
  }

  async update() {
    try {
      debug("Updating data-base developments");

      if (!this.isUpdating) {
        this.isUpdating = true;

        const dolarValue = await new DolarAPI().getDolar();
        const tokkoApi = new TokkoAPI();
        const deverlopmentsAmount = await tokkoApi.getDevelopmentsAmount();

        debug(`Tokko developments amount: ${deverlopmentsAmount}`);

        const updateDevelopmentsBatchPromises = [];

        for (
          let offset = 0;
          offset <= deverlopmentsAmount + TokkoAPI.DEVELOPMENTS_LIMIT;
          offset = offset + TokkoAPI.DEVELOPMENTS_LIMIT
        ) {
          updateDevelopmentsBatchPromises.push(
            this.updateDevelopmentsBatch(offset, dolarValue)
          );
        }

        await Promise.all(updateDevelopmentsBatchPromises);

        this.progress = 0;
        this.isUpdating = false;
      }
    } catch (error) {
      this.isUpdating = false;
      this.progress = 0;

      throw error;
    }
  }
}
