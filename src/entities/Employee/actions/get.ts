import { RequestHandler } from "express";

const debug = require("debug")("actions:Employee:get");

export const get: RequestHandler = async (req, res, next) => {
  try {
    debug("Calling action");

    const { password, ...employee } = (await req.employee.get()).toObject();

    debug(`Succesful ✅`);

    res.status(200);
    res.send(employee);
  } catch (error) {
    debug(`❌ Something went wrong`);

    next(error);
  }
};
