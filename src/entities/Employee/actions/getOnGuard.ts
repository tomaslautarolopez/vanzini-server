import { RequestHandler } from "express";
import { Employee } from "../Employee";

const debug = require("debug")("actions:Employee:getOnGuard");

export const getOnGuard: RequestHandler = async (req, res, next) => {
  try {
    debug("Calling action");

    const employee = await Employee.getOnGuardEmployee();

    debug(`Succesful ✅`);

    res.status(200);
    res.send(employee?.toJSON());
  } catch (error) {
    debug(`❌ Something went wrong`);

    next(error);
  }
};
