import { RequestHandler } from "express";
import { Employee } from "../Employee";

const debug = require("debug")("actions:Employee:deleteE");

export const deleteE: RequestHandler = async (req, res, next) => {
  try {
    debug("Calling action");

    await new Employee(req.params.employeeId).delete();

    debug(`Succesful ✅`);

    res.status(200);
    res.send("ok");
  } catch (error) {
    debug(`❌ Something went wrong`);

    next(error);
  }
};
