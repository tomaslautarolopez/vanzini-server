import { RequestHandler } from "express";
import { Employee } from "../Employee";

const debug = require("debug")("actions:Employee:update");

export const update: RequestHandler = async (req, res, next) => {
  try {
    const {
      phonenumber,
      name,
      employeeType,
      isInRoulette,
      role,
      pictureUrl,
    } = req.body;
    const { employeeId } = req.params;

    debug("Calling action");

    await new Employee(employeeId).update(
      phonenumber,
      name,
      employeeType,
      isInRoulette,
      role,
      pictureUrl
    );

    debug(`Succesful ✅`);

    res.status(200);
    res.send("ok");
  } catch (error) {
    debug(`❌ Something went wrong`);

    next(error);
  }
};
