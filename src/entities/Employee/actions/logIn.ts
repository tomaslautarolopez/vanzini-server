import { RequestHandler } from "express";
import { Employee } from "../Employee";

const debug = require("debug")("actions:Employee:logIn");

export const logIn: RequestHandler = async (req, res, next) => {
  const { usernameOrEmail, password } = req.body;
  try {
    debug("Calling action");

    const token = await new Employee(
      undefined,
      usernameOrEmail
    ).getEmployeeToken(password);

    debug(`Succesful ✅`);

    res.status(200);
    res.send({
      token,
    });
  } catch (error) {
    debug(`❌ Something went wrong`);

    next(error);
  }
};
