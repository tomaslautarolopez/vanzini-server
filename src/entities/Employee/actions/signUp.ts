import { RequestHandler } from "express";
import { Employee } from "../Employee";

const debug = require("debug")("actions:Employee:signUp");

export const signUp: RequestHandler = async (req, res, next) => {
  try {
    debug("Calling action");

    const { password, ...employee } = await (
      await Employee.create(req.body)
    ).toJSON();

    debug(`Succesful ✅`);

    res.status(200);
    res.send(employee);
  } catch (error) {
    debug(`❌ Something went wrong`);
    next(error);
  }
};
