import { RequestHandler } from "express";
import { Employee } from "../Employee";

const debug = require("debug")("actions:Employee:getWebEmployees");

export const getWebEmployees: RequestHandler = async (req, res, next) => {
  try {
    debug("Calling action");

    const employees = await Employee.getWebEmployees();

    debug(`Succesful ✅`);

    res.status(200);
    res.send(employees);
  } catch (error) {
    debug(`❌ Something went wrong`);

    next(error);
  }
};
