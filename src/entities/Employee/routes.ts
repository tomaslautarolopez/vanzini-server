import { Router } from "express";
import { update } from "./actions/update";
import { deleteE } from "./actions/deleteE";
import { get } from "./actions/get";
import { getOnGuard } from "./actions/getOnGuard";
import { getWebEmployees } from "./actions/getWebEmployees";
import { logIn } from "./actions/logIn";
import { signUp } from "./actions/signUp";
import { hasRole } from "./middlewares/hasRole";
import { isLoggedIn } from "./middlewares/isLoggedIn";
import { Role } from "./models/EmployeeModel";

export const employeeRouter = Router();

export const employeePrefixUrl = "/employee";

employeeRouter.put("/:employeeId", isLoggedIn, hasRole(Role.ADMIN), update);

employeeRouter.post("/", isLoggedIn, hasRole(Role.ADMIN), signUp);
employeeRouter.post("/log-in", logIn);

employeeRouter.get("/", isLoggedIn, get);
employeeRouter.get("/get-on-guard", getOnGuard);
employeeRouter.get("/get-web", getWebEmployees);

employeeRouter.delete("/:employeeId", isLoggedIn, hasRole(Role.ADMIN), deleteE);
