import {
  prop,
  getModelForClass,
  modelOptions,
  index,
} from "@typegoose/typegoose";
import * as mongoose from "mongoose";

export enum Role {
  ADMIN = "ADMIN",
  USER = "USER",
}

@index({ username: "text" })
@index({ username: "email" })
@modelOptions({ schemaOptions: { collection: "employees" } })
export class EmployeeSchema {
  @prop()
  public showInWeb?: boolean;

  @prop({ unique: true, required: true })
  username?: string;

  @prop({ required: true })
  email: string;

  @prop({ required: true })
  phonenumber?: string;

  @prop({ required: true })
  name?: string;

  @prop({ required: true })
  password: string;

  @prop()
  onGuard?: boolean;

  @prop()
  employeeType?: string; // This is the role of the employee inside the organization

  @prop()
  isInRoulette?: boolean;

  @prop({ default: Role.USER, enum: Role, type: String })
  role?: Role; // This is the permission of the employee inside the application

  @prop()
  pictureUrl?: string;
}

export const EmployeeModel = getModelForClass(EmployeeSchema, {
  schemaOptions: { timestamps: true },
});
