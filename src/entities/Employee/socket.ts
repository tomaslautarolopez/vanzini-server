import io from "socket.io";
import { verify } from "jsonwebtoken";

import { ioSocket } from "../../server";
import { encryptionKey } from "../../config/config";
import { CustomError } from "../../errorHandling/CustomError";
import { EMPLOYEE_ERRORS } from "./error";
import { Employee } from "./Employee";
import { updateAllAction } from "../BaseEstate/actions/updateAll";

export const EMPLOYEE_BASE_ESTATE_ROOM = "employee-properties-room";

const debug = require("debug")("socket:Employee");

export class EmployeeSocket {
  namespace: io.Namespace = ioSocket.of("/employee");
  private static instance: EmployeeSocket;

  constructor() {
    if (!!EmployeeSocket.instance) {
      return EmployeeSocket.instance;
    }

    EmployeeSocket.instance = this;

    this.init();

    return this;
  }

  private init() {
    this.namespace.use(this.authMiddleware);

    this.namespace.on("connection", (socket) => {
      debug("Employee conected to employee socket");

      socket.join(EMPLOYEE_BASE_ESTATE_ROOM);
      socket.emit("connection", "Succesful connection");

      this.actions(socket);
    });
  }

  sendMessageToEmployeeRoom(event: string, message: any) {
    this.namespace.to(EMPLOYEE_BASE_ESTATE_ROOM).emit(event, message);
  }

  private actions(socket: io.Socket) {
    socket.on("update-all", () => {
      debug("Updating properties by socket");
      updateAllAction();
    });
  }

  private async authMiddleware(socket: io.Socket, next: (err?: any) => void) {
    const token: string = socket.handshake.auth.token;

    const decoded = verify(token, encryptionKey);

    if (!decoded || typeof decoded === "string" || !(decoded as any).username) {
      return next(
        new CustomError(
          401,
          "Invalid token provided",
          EMPLOYEE_ERRORS.INVALID_TOKEN
        )
      );
    }

    const employee = await new Employee(
      undefined,
      (decoded as any).username
    ).get();

    if (employee) {
      return next();
    } else {
      socket.disconnect(true);
      return next(
        new CustomError(404, "Employee not found", EMPLOYEE_ERRORS.NOT_FOUND)
      );
    }
  }
}
