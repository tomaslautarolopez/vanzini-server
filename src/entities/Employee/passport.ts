import passport from "passport";
import { Strategy as JWTStrategy, ExtractJwt } from "passport-jwt";
import { encryptionKey } from "../../config/config";
import { CustomError } from "../../errorHandling/CustomError";
import { Employee } from "./Employee";
import { EMPLOYEE_ERRORS } from "./error";

passport.use(
  "jwt-employee",
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: encryptionKey,
    },
    async (jwtPayload: any, done: Function) => {
      try {
        const employee = new Employee(jwtPayload.id);

        if (!(await employee.exists())) {
          return done(
            new CustomError(
              401,
              "Invalid token",
              EMPLOYEE_ERRORS.INVALID_TOKEN
            ),
            undefined
          );
        }

        return done(null, employee);
      } catch (error) {
        done(error);
      }
    }
  )
);
