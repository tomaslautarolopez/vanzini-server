import { NextFunction, Request, Response } from "express";
import { authenticate } from "passport";
import { CustomError } from "../../../errorHandling/CustomError";
import { Employee } from "../Employee";
import { EMPLOYEE_ERRORS } from "../error";

export const isLoggedIn = (
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  return authenticate(
    "jwt-employee",
    { session: false },
    (error: Error, employee: Employee) => {
      if (error instanceof CustomError) {
        next(error);
      }

      if (error || !employee) {
        next(
          new CustomError(
            500,
            "Something went wrong while authenticating",
            EMPLOYEE_ERRORS.FAILED_AUTHENTICATION
          )
        );
      }

      req.employee = employee;
      next();
    }
  )(req, res, next);
};
