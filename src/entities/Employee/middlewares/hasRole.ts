import { NextFunction, Request, Response } from "express";
import { CustomError } from "../../../errorHandling/CustomError";
import { EMPLOYEE_ERRORS } from "../error";
import { Role } from "../models/EmployeeModel";

export const hasRole = (role: Role) => async (
  req: Request,
  _res: Response,
  next: NextFunction
): Promise<void> => {
  const employee = await req.employee.get();

  if (employee.role === role) {
    next();
  } else {
    next(new CustomError(
      401,
      "You dont have the permission to perform this action",
      EMPLOYEE_ERRORS.UNAUTHORIZED
    ));
  }
};
