import { ObjectId } from "bson";
import { sign } from "jsonwebtoken";
import bcrypt from "bcrypt";
import { FilterQuery } from "mongoose";
import { CustomError } from "../../errorHandling/CustomError";
import { EMPLOYEE_ERRORS } from "./error";
import { EmployeeModel, EmployeeSchema, Role } from "./models/EmployeeModel";
import { DocumentType } from "@typegoose/typegoose";
import { cleanUndefinedValuesInObject } from "../../utils/cleanUndefinedValuesInObject";
import { encryptionKey } from "../../config/config";
import "./passport";

const debug = require("debug")("entities:Employee");

export class Employee {
  id?: ObjectId;
  usernameOrEmail?: string;
  chache?: DocumentType<EmployeeSchema> | null;

  constructor(id?: string, usernameOrEmail?: string) {
    if ((!id && !usernameOrEmail) || (id && !ObjectId.isValid(id)))
      throw new CustomError(
        400,
        "Invalid employee Id",
        EMPLOYEE_ERRORS.INVALID_ID
      );

    this.usernameOrEmail = usernameOrEmail;
    this.id = id ? new ObjectId(id) : undefined;
  }

  async update(
    phonenumber: string | undefined,
    name: string | undefined,
    employeeType: string | undefined,
    isInRoulette: boolean | undefined,
    role: Role | undefined,
    pictureUrl: string | undefined
  ) {
    const update = cleanUndefinedValuesInObject({
      phonenumber,
      name,
      employeeType,
      isInRoulette,
      role,
      pictureUrl,
    });

    const filter: FilterQuery<
      DocumentType<EmployeeSchema>
    > = cleanUndefinedValuesInObject({
      _id: this.id,
      $or: this.usernameOrEmail && [
        {
          username: this.usernameOrEmail,
        },
        {
          email: this.usernameOrEmail,
        },
      ],
    });

    await EmployeeModel.updateOne({ $set: filter }, update);
  }

  async delete() {
    const filter: FilterQuery<
      DocumentType<EmployeeSchema>
    > = cleanUndefinedValuesInObject({
      _id: this.id,
      $or: this.usernameOrEmail && [
        {
          username: this.usernameOrEmail,
        },
        {
          email: this.usernameOrEmail,
        },
      ],
    });

    return await EmployeeModel.deleteOne(filter);
  }

  async exists() {
    try {
      return !!(await this.get());
    } catch (error) {
      return false;
    }
  }

  async get(skipCache?: boolean) {
    if (this.chache && !skipCache) {
      return this.chache;
    }

    const filter: FilterQuery<
      DocumentType<EmployeeSchema>
    > = cleanUndefinedValuesInObject({
      _id: this.id,
      $or: this.usernameOrEmail && [
        {
          username: this.usernameOrEmail,
        },
        {
          email: this.usernameOrEmail,
        },
      ],
    });

    const employee = await EmployeeModel.findOne(filter);

    if (!employee) {
      throw new CustomError(
        400,
        "Employee not found",
        EMPLOYEE_ERRORS.NOT_FOUND
      );
    }

    this.chache = employee;

    return employee;
  }

  async getEmployeeToken(password: string) {
    const employee = await this.get();

    if (!employee) {
      throw new CustomError(
        400,
        "Employee not found",
        EMPLOYEE_ERRORS.NOT_FOUND
      );
    }

    const passwordsMatch = await bcrypt.compare(password, employee.password);

    if (!passwordsMatch) {
      throw new CustomError(
        401,
        "Employee not found",
        EMPLOYEE_ERRORS.INVALID_USERNAME_OR_PASSWORD
      );
    }

    const payload = {
      id: employee.id,
    };

    const token = sign(payload, encryptionKey);

    return token;
  }

  static async create(employee: EmployeeSchema) {
    debug({ employee });

    const alreadyExistsUsername = await new Employee(
      undefined,
      employee.username
    ).exists();

    if (alreadyExistsUsername) {
      throw new CustomError(
        400,
        "The username is already in use",
        EMPLOYEE_ERRORS.ALREADY_USED_USERNAME
      );
    }

    const saltRounds: number = 10;
    const hashedPassword: string = await bcrypt.hash(
      employee.password,
      saltRounds
    );

    const newEmployee = await EmployeeModel.create({
      ...employee,
      password: hashedPassword,
    });

    return newEmployee;
  }

  static async getAll() {
    return await EmployeeModel.findOne({}).select({
      password: 0,
    });
  }

  static async getOnGuardEmployee() {
    return await EmployeeModel.findOne({
      onGuard: true,
    }).select({
      password: 0,
      isInRoulette: 0,
      role: 0,
    });
  }

  static async getWebEmployees() {
    return await EmployeeModel.find({
      showInWeb: true,
    }).select({ password: 0, isInRoulette: 0, role: 0 });
  }
}
