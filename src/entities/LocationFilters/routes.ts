import { Router } from "express";
import { get } from "./actions/get";

export const locationFiltersRouter = Router();

export const locationFiltersPrefixUrl = "/location-filters";

locationFiltersRouter.get("/", get);
