import { RequestHandler } from "express";
import { LocationsFilters } from "../../LocationFilters/LocationsFilters";

const debug = require("debug")("actions:LocationFilters:get");

export const get: RequestHandler = async (_req, res, next) => {
  try {
    debug("Calling action");

    const locationsFilters = await new LocationsFilters().get();

    debug("Succesful ✅");

    res.status(200);
    res.send(locationsFilters);
  } catch (error) {
    debug("❌ Something went wrong");

    next(error);
  }
};
