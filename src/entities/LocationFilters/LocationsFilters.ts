import { BaseEstateModel } from "../BaseEstate/models/BaseEstateModel";
import { set, get } from "lodash";

export class LocationsFilters {
  private locationOptions: null | object = null;
  private static instance: LocationsFilters;

  constructor() {
    if (!!LocationsFilters.instance) {
      return LocationsFilters.instance;
    }

    LocationsFilters.instance = this;
    this.locationOptions = null;

    return this;
  }

  async get() {
    if (!this.locationOptions) {
      await this.generate();
    }

    return this.locationOptions;
  }

  async generate() {
    const locationOptions = {};
    const propertiesLocations = await BaseEstateModel.find(
      {},
      {
        _id: 1,
        country: 1,
        firstSubDivision: 1,
        secondSubDivision: 1,
        developmentProperties: 1,
      }
    );

    propertiesLocations.forEach((baseEstate) => {
      const {
        country,
        firstSubDivision,
        secondSubDivision,
        developmentProperties,
      } = baseEstate;

      const countryPath = country || "";
      const firstSubDivisionPath =
        firstSubDivision && countryPath.length
          ? `${countryPath}.${firstSubDivision}`
          : "";
      const secondSubDivisionPath =
        secondSubDivision && firstSubDivisionPath.length
          ? `${firstSubDivisionPath}.${secondSubDivision}`
          : "";

      [countryPath, firstSubDivisionPath, secondSubDivisionPath].forEach(
        (path: string) => {
          if (path.length) {
            const plus =
              developmentProperties && developmentProperties.length
                ? developmentProperties.length
                : 1;
            const amount = get(locationOptions, `${path}.amount`, 0);
            set(locationOptions, `${path}.amount`, amount + plus);
          }
        }
      );
    });

    this.locationOptions = locationOptions;

    return locationOptions;
  }
}
