import { cdnImageKey } from "../../config/config";
import { TAGS } from "../../constants";
import { BaseEstate } from "../BaseEstate/BaseEstate";
import { TOKKO_URL } from "../../APIs/TokkoAPI";
import { hasTags } from "../../utils/hasTags";
import { createCustomTag } from "../../utils/createCustomTag";
import { slugAndEncode } from "../../utils/slugAndEncode";
import { BaseEstateModel } from "../BaseEstate/models/BaseEstateModel";

export class Property extends BaseEstate {
  usdExchange: number;

  constructor(id: string, usdExchange: number, rawData?: any) {
    super(id);
    this.rawData = rawData;
    this.tokkoUrl = TOKKO_URL.PROPERTY;
    this.usdExchange = usdExchange;
  }

  async prepareForDb(rawData: any) {
    const photos =
      rawData.photos &&
      rawData.photos.map((photo: any) => ({
        ...photo,
        image: `${cdnImageKey}${photo.image.split("/").slice(-2).join("/")}`,
      }));

    const location = rawData.location;
    const splitedLocation = location.full_location
      .split(" | ")
      .map((entry: string) => entry.trim().replace(".", " "));

    const firstSubDivision =
      splitedLocation.length < 3
        ? splitedLocation[1]
        : splitedLocation[splitedLocation.length - 2];
    const secondSubDivision =
      splitedLocation.length < 3
        ? splitedLocation[2]
        : splitedLocation[splitedLocation.length - 1];

    const geoLocation = {
      type: "Point",
      coordinates: [parseFloat(rawData.geo_lat), parseFloat(rawData.geo_long)],
    };

    return new BaseEstateModel({
      _id: rawData.id.toString(),
      roomAmount: rawData.suite_amount,
      ambientAmount: rawData.room_amount,
      bathroomAmount: rawData.bathroom_amount,
      toiletAmount: rawData.toilet_amount,
      garageAmount: rawData.parking_lot_amount,
      floorsAmount: rawData.floors_amount,
      publicationTitle: rawData.publication_title,
      situation: rawData.situation,
      disposition: rawData.disposition || "",
      orientation: rawData.orientation || "",
      expenses: rawData.expenses,
      hasGarage:
        rawData.parking_lot_amount > 0 ||
        hasTags(rawData.tags, [TAGS.COCHERA, TAGS.COCHERA_OPCIONAL]),
      age: rawData.age,
      development: rawData.development,
      zonification: rawData.zonification,
      constructionStatus: rawData.status,
      webPrice: rawData.web_price === undefined ? true : rawData.web_price,
      geoLocation,
      sell: !!rawData.operations.find(
        (op: any) => op.operation_type === "Venta"
      ),
      rent: !!rawData.operations.find(
        (op: any) => op.operation_type === "Alquiler"
      ),
      creationDate: new Date(rawData.created_at).getTime(),
      country: splitedLocation[0],
      firstSubDivision,
      secondSubDivision,
      producer: rawData.producer,
      operationSell: BaseEstate.sellPriceGenerator(
        rawData.operations,
        this.usdExchange
      ),
      operationRent: BaseEstate.rentPriceGenerator(
        rawData.operations,
        this.usdExchange
      ),
      unroofedSurface: Number(rawData.unroofed_surface),
      roofedSurface: Number(rawData.roofed_surface),
      semiroofedSurface: Number(rawData.semiroofed_surface),
      totalSurfaceMin: Number(rawData.total_surface),
      totalSurfaceMax: Number(rawData.total_surface),
      surface: Number(rawData.surface),
      description: rawData.description,
      tags: rawData.tags.map((tag: any) => tag.name),
      tagsInfo: rawData.tags,
      customTags: createCustomTag(rawData.custom_tags),
      customTagsInfo: rawData.custom_tags,
      address: rawData.fake_address,
      addressSlug: slugAndEncode(rawData.fake_address),
      baseEstateType: rawData.type.code,
      photos,
      image360: rawData.videos.length > 0 ? rawData.videos[0].url : undefined,
      propertyCondition: rawData.property_condition || "No disponible",
    });
  }
}
