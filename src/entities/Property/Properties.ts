import { DolarAPI } from "../../APIs/DolarAPI";
import { TokkoAPI } from "../../APIs/TokkoAPI";
import { EmployeeSocket } from "../Employee/socket";
import { Property } from "./Property";

const debug = require("debug")("entity:Properties");

export class Properties {
  isUpdating: boolean;
  progress: number;
  private static instance: Properties | null;
  private tokkoApi: TokkoAPI;

  constructor() {
    if (!!Properties.instance) {
      return Properties.instance;
    }

    Properties.instance = this;
    this.isUpdating = false;
    this.progress = 0;
    this.tokkoApi = new TokkoAPI();

    return this;
  }

  private async updatePropertiesBatch(offset: number, dolarValue: number) {
    const {
      tokkoProperties,
      propertiesAmount,
    } = await this.tokkoApi.getBatchProperties(offset);
    const employeeSocket = new EmployeeSocket();

    await Promise.all(
      tokkoProperties.map((tokkoProperty: any) =>
        new Property(
          tokkoProperty.id.toString(),
          dolarValue,
          tokkoProperty
        ).update()
      )
    );

    this.progress = Math.min(
      this.progress + (TokkoAPI.PROPERTIES_LIMIT / propertiesAmount) * 100,
      100
    );
    employeeSocket.sendMessageToEmployeeRoom(
      "properties-progress",
      this.progress
    );

    debug("Finish updating data-base properties batch");
    debug(`Offset: ${offset}`);
    debug(`Progress: ${this.progress}`);
  }

  async update() {
    try {
      debug("Updating data-base properties");

      if (!this.isUpdating) {
        this.isUpdating = true;

        const dolarValue = await new DolarAPI().getDolar();
        const tokkoApi = new TokkoAPI();
        const propertiesAmount = await tokkoApi.getPropertiesAmount();

        debug(`Tokko properties amount: ${propertiesAmount}`);

        const updatePropertiesBatchPromises = [];

        for (
          let offset = 0;
          offset <= propertiesAmount + TokkoAPI.PROPERTIES_LIMIT;
          offset = offset + TokkoAPI.PROPERTIES_LIMIT
        ) {
          updatePropertiesBatchPromises.push(
            this.updatePropertiesBatch(offset, dolarValue)
          );
        }

        await Promise.all(updatePropertiesBatchPromises);

        this.progress = 0;
        this.isUpdating = false;
      }
    } catch (error) {
      this.isUpdating = false;
      this.progress = 0;

      throw error;
    }
  }
}
