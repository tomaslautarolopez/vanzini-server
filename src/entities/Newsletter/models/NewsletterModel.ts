import {
  prop,
  getModelForClass,
  modelOptions,
} from "@typegoose/typegoose";
import * as mongoose from "mongoose";

@modelOptions({ schemaOptions: { collection: "newsletter" } })
export class NewsletterSchema {
  @prop({ required: true })
  suscribed?: boolean;

  @prop({ required: true })
  email: string;
}

export const NewsletterModel = getModelForClass(NewsletterSchema, {
  schemaOptions: { timestamps: true },
});
