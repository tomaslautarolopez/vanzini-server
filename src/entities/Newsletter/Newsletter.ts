import { NewsletterModel } from "./models/NewsletterModel";

export class Newsletter {
  static getSuscribers() {
    return NewsletterModel.find({
      suscribed: true,
    });
  }

  static async isSuscribed(email: string) {
    return !!NewsletterModel.findOne({
      email,
    });
  }

  static async suscribe(email: string) {
    await NewsletterModel.updateOne(
      {
        email,
      },
      {
        $set: {
          suscribed: true,
        },
      },
      {
        upsert: true,
      }
    );
  }

  static async unsuscribe(email: string) {
    await NewsletterModel.updateOne(
      {
        email,
      },
      {
        $set: {
          suscribed: false,
        },
      }
    );
  }
}
