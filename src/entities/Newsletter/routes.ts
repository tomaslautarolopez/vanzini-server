import { Router } from "express";
import { isLoggedIn } from "../Employee/middlewares/isLoggedIn";
import { getSuscribers } from "./actions/getSuscribers";
import { isSuscribed } from "./actions/isSuscribed";
import { suscribe } from "./actions/suscribe";
import { unsuscribe } from "./actions/unsuscribe";

export const newsletterRouter = Router();

export const newsletterPrefixUrl = "/newsletter";

newsletterRouter.get("/", isLoggedIn, getSuscribers);
newsletterRouter.get("/is-suscribed", isSuscribed);

newsletterRouter.post("/", suscribe);

newsletterRouter.put("/", unsuscribe);
