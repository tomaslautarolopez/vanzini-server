import { RequestHandler } from "express";
import { Newsletter } from "../Newsletter";

const debug = require("debug")("actions:Newsletter:isSuscribed");

export const isSuscribed: RequestHandler = async (req, res, next) => {
  try {
    debug("Calling action");

    await Newsletter.isSuscribed(req.body.email);

    debug("Succesful ✅");

    res.status(200);
    res.send("ok");
  } catch (error) {
    debug("❌ Something went wrong");

    next(error);
  }
};
