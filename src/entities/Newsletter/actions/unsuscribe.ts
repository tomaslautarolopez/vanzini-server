import { RequestHandler } from "express";
import { Newsletter } from "../Newsletter";

const debug = require("debug")("actions:Newsletter:unsuscribe");

export const unsuscribe: RequestHandler = async (req, res, next) => {
  try {
    debug("Calling action");

    await Newsletter.unsuscribe(req.body.email);

    debug("Succesful ✅");

    res.status(200);
    res.send("ok");
  } catch (error) {
    debug("❌ Something went wrong");

    next(error);
  }
};
