import { RequestHandler } from "express";
import { Newsletter } from "../Newsletter";

const debug = require("debug")("actions:Newsletter:getSuscribers");

export const getSuscribers: RequestHandler = async (_req, res, next) => {
  try {
    debug("Calling action");

    const suscribers = await Newsletter.getSuscribers();

    const file: string = suscribers
      .map((suscriber) => suscriber.email)
      .join("\n");

    debug("Succesful ✅");

    res.status(200);
    res.set({
      "Content-Disposition": `attachment; filename=newsletterSubs.txt`,
    });
    res.send(Buffer.from(file));
  } catch (error) {
    debug("❌ Something went wrong");

    next(error);
  }
};
