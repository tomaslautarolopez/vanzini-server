import {
  prop,
  getModelForClass,
  modelOptions,
  index,
  Ref,
} from "@typegoose/typegoose";
import * as mongoose from "mongoose";
import { EmployeeSchema } from "../../Employee/models/EmployeeModel";

@index({ slug: "text" })
@modelOptions({ schemaOptions: { collection: "banners" } })
export class BannerSchema {
  @prop()
  public code?: string;

  @prop()
  public title?: string;

  @prop({ ref: () => EmployeeSchema })
  public createdBy?: Ref<EmployeeSchema>;

  @prop()
  public showInWeb?: boolean;
}

export const BannerModel = getModelForClass(BannerSchema, {
  schemaOptions: { timestamps: true },
});
