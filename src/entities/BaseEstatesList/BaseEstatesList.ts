import { DocumentType } from "@typegoose/typegoose";
import { ObjectId } from "bson";
import { CustomError } from "../../errorHandling/CustomError";
import { slugAndEncode } from "../../utils/slugAndEncode";
import { BASE_ESTATES_LIST_ERRORS } from "./error";
import {
  BaseEstatesListModel,
  BaseEstatesListSchema,
} from "./models/BaseEstatesListModel";
import { Types, FilterQuery } from "mongoose";
import { Employee } from "../Employee/Employee";
import { BaseEstatesListFilters } from "./interfaces";
import { Sort } from "../../utils/createSortObject";
import { Projection } from "../../utils/createProjectionObject";
import { cleanUndefinedValuesInObject } from "../../utils/cleanUndefinedValuesInObject";

export class BaseEstatesList {
  id?: ObjectId;
  slug?: string;
  chache?: DocumentType<BaseEstatesListSchema> | null;

  constructor(id?: string, slug?: string) {
    if ((!id && !slug) || (id && !ObjectId.isValid(id)))
      throw new CustomError(
        400,
        "Invalid base estate list Id",
        BASE_ESTATES_LIST_ERRORS.INVALID_ID
      );

    this.slug = slug;

    this.id = id ? new Types.ObjectId(id) : undefined;
  }

  async get(skipCache?: boolean): Promise<DocumentType<BaseEstatesListSchema>> {
    if (this.chache && !skipCache) {
      return this.chache;
    }

    const filter: FilterQuery<DocumentType<BaseEstatesListSchema>> = {};

    if (this.id) {
      filter._id = this.id;
    }

    if (this.slug) {
      filter.slug = this.slug;
    }

    const baseEstateList = await BaseEstatesListModel.findOne(filter).populate(
      "list"
    );

    if (baseEstateList) {
      this.chache = baseEstateList;

      return baseEstateList;
    } else {
      throw new CustomError(
        404,
        "Base estate list not found",
        BASE_ESTATES_LIST_ERRORS.NOT_FOUND
      );
    }
  }

  async delete() {
    const filter: FilterQuery<DocumentType<BaseEstatesListSchema>> = {};

    if (this.id) {
      filter._id = this.id;
    }

    if (this.slug) {
      filter.slug = this.slug;
    }

    const { deletedCount } = await BaseEstatesListModel.deleteOne(filter);

    if (deletedCount === 0) {
      throw new CustomError(
        404,
        "Base estate list not found",
        BASE_ESTATES_LIST_ERRORS.NOT_FOUND
      );
    }
  }

  static async create(
    title: string,
    description: string,
    list: string[],
    portrait?: string,
    showInWeb?: boolean,
    employee?: Employee
  ) {
    const baseEstateList = await BaseEstatesListModel.create({
      title,
      description,
      list,
      portrait,
      showInWeb,
      createdBy: (await employee?.get())?._id,
      slug: slugAndEncode(title),
    });

    return baseEstateList;
  }

  static titleFilterGenerator(title?: string) {
    if (title) {
      return {
        $text: {
          $search: slugAndEncode(title),
        },
      };
    } else {
      return {};
    }
  }

  static idsFilterGenerator(ids?: string[]) {
    if (ids) {
      return {
        $in: ids,
      };
    } else {
      return undefined;
    }
  }

  static listFilterGenerator(list?: string[]) {
    if (list) {
      return {
        $all: [...list],
      };
    } else {
      return undefined;
    }
  }

  static filtersGenerator(filters: BaseEstatesListFilters) {
    const { ids, title, list, showInWeb } = filters;

    const titleFilter = this.titleFilterGenerator(title);
    const idsFilter = this.idsFilterGenerator(ids);
    const listFilter = this.listFilterGenerator(list);

    return {
      ...titleFilter,
      _id: idsFilter,
      list: listFilter,
      showInWeb,
    };
  }

  static async getBatch(
    filters: BaseEstatesListFilters,
    sort: Sort,
    offset: number,
    projection: Projection | undefined = {},
    limit: number | undefined
  ): Promise<{
    baseEstatesLists: BaseEstatesListSchema[];
    totalCount: number;
  }> {
    const dbfilters = cleanUndefinedValuesInObject(
      this.filtersGenerator(filters)
    );

    const dbOptions = {
      skip: offset,
      limit: limit,
    };

    if (filters.title) {
      projection.score = { $meta: "textScore" };
      sort.score = { $meta: "textScore" };
    }

    const [baseEstatesLists, totalCount] = await Promise.all([
      BaseEstatesListModel.find(dbfilters, projection, dbOptions)
        .sort({
          ...sort,
        })
        .populate("list"),
      BaseEstatesListModel.countDocuments(dbfilters),
    ]);

    return {
      baseEstatesLists,
      totalCount,
    };
  }
}
