export interface BaseEstatesListFilters {
  list?: string[];
  title?: string;
  showInWeb?: boolean;
  ids?: string[];
}

export interface QueryFilters {
  list?: string[];
  title?: string;
  showInWeb?: string;
  ids?: string[];
  fields?: string[];
  sort?: string;
  limit?: string;
  offset?: string;
}
