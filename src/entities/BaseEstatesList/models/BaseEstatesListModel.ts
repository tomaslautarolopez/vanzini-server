import {
  prop,
  getModelForClass,
  modelOptions,
  index,
  Ref,
} from "@typegoose/typegoose";
import * as mongoose from "mongoose";
import { BaseEstateSchema } from "../../BaseEstate/models/BaseEstateModel";
import { EmployeeSchema } from "../../Employee/models/EmployeeModel";

@index({ slug: "text" })
@modelOptions({ schemaOptions: { collection: "properties-lists" } })
export class BaseEstatesListSchema {
  @prop()
  public description?: string;

  @prop()
  public title?: string;

  @prop({ unique: true })
  public slug?: string;

  @prop()
  public portrait?: string;

  @prop({ ref: () => BaseEstateSchema, type: String })
  public list?: Ref<BaseEstateSchema>[];

  @prop({ ref: () => EmployeeSchema })
  public createdBy?: Ref<EmployeeSchema>;

  @prop()
  public showInWeb?: boolean;
}

export const BaseEstatesListModel = getModelForClass(BaseEstatesListSchema, {
  schemaOptions: { timestamps: true },
});
