import { RequestHandler } from "express";
import { BaseEstatesList } from "../BaseEstatesList";

const debug = require("debug")("actions:BaseEstatesList:create");

export const create: RequestHandler = async (req, res, next) => {
  const { title, description, list, portrait, showInWeb } = req.body;
  const { employee } = req;

  try {
    debug("Calling action");

    const baseEstatesList = await BaseEstatesList.create(
      title,
      description,
      list,
      portrait,
      showInWeb,
      employee
    );

    debug(`Succesful ✅ ${baseEstatesList.id}`);

    res.status(200);
    res.send(baseEstatesList);
  } catch (error) {
    debug(`❌ Something went wrong ${title}`);

    next(error);
  }
};
