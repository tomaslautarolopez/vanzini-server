import { RequestHandler } from "express";
import { BaseEstatesList } from "../BaseEstatesList";

const debug = require("debug")("actions:BaseEstatesList:get");

export const get: RequestHandler = async (req, res, next) => {
  const { baseEstatesListId, baseEstatesListSlug } = req.params;

  try {
    debug("Calling action");

    const baseEstatesList = await new BaseEstatesList(
      baseEstatesListId,
      baseEstatesListSlug
    ).get();

    debug(`Succesful ✅ ${baseEstatesListId || baseEstatesListSlug}`);

    res.status(200);
    res.send(baseEstatesList);
  } catch (error) {
    debug(
      `❌ Something went wrong ${baseEstatesListId || baseEstatesListSlug}`
    );

    next(error);
  }
};
