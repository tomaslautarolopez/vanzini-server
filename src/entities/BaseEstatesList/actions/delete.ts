import { RequestHandler } from "express";
import { BaseEstatesList } from "../BaseEstatesList";

const debug = require("debug")("actions:BaseEstatesList:deleteBEL");

export const deleteBEL: RequestHandler = async (req, res, next) => {
  const { baseEstatesListId, baseEstatesListSlug } = req.params;

  try {
    debug("Calling action");

    await new BaseEstatesList(baseEstatesListId, baseEstatesListSlug).delete();

    debug(`Succesful ✅ ${baseEstatesListId || baseEstatesListSlug}`);

    res.status(200);
    res.send("Succesful delete");
  } catch (error) {
    debug(
      `❌ Something went wrong ${baseEstatesListId || baseEstatesListSlug}`
    );

    next(error);
  }
};
