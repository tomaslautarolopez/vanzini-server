import { RequestHandler } from "express";
import { createProjectionObject } from "../../../utils/createProjectionObject";
import { createSortObject } from "../../../utils/createSortObject";
import { BaseEstatesList } from "../BaseEstatesList";
import { BaseEstatesListFilters, QueryFilters } from "../interfaces";

const debug = require("debug")("actions:BaseEstatesList:getAll");

export const getAll: RequestHandler<any, any, any, QueryFilters> = async (
  req,
  res,
  next
) => {
  const {
    showInWeb,
    title,
    list,
    ids,
    fields,
    sort: sortString,
    limit: limitString,
    offset: offsetString,
  } = req.query;

  const filter: BaseEstatesListFilters = {
    showInWeb: showInWeb ? true : undefined,
    title,
    list,
    ids,
  };
  const projection = fields && createProjectionObject(fields);
  const sort = sortString ? createSortObject(sortString) : { creationDate: -1 };
  const limit = limitString ? Number(limitString) : undefined;
  const offset = offsetString ? Number(offsetString) : 0;

  try {
    debug("Calling action");

    const { baseEstatesLists, totalCount } = await BaseEstatesList.getBatch(
      filter,
      sort,
      offset,
      projection,
      limit
    );

    debug("Succesful ✅");
    debug({ totalCount });

    res.status(200);
    res.send({
      baseEstatesLists,
      totalCount,
    });
  } catch (error) {
    debug("❌ Something went wrong");

    next(error);
  }
};
