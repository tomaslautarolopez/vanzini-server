import { Router } from "express";
import { isLoggedIn } from "../Employee/middlewares/isLoggedIn";
import { create } from "./actions/create";
import { deleteBEL } from "./actions/delete";
import { get } from "./actions/get";
import { getAll } from "./actions/getAll";

export const baseEstatesListRouter = Router();

export const baseEstatesListPrefixUrl = "/base-estates-list";

baseEstatesListRouter.post("/", isLoggedIn, create);

baseEstatesListRouter.get("/", getAll);
baseEstatesListRouter.get("/id/:baseEstatesListId", get);
baseEstatesListRouter.get("/slug/:baseEstatesListSlug", get);

baseEstatesListRouter.delete("/id/:baseEstatesListId", isLoggedIn, deleteBEL);
baseEstatesListRouter.delete(
  "/slug/:baseEstatesListSlug",
  isLoggedIn,
  deleteBEL
);
