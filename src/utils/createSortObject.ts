export interface Sort {
  [x: string]: number | { $meta: "textScore" };
}

const sortMap: { [key: string]: number } = {
  ASC: 1,
  DESC: -1,
};

export const createSortObject = (sort: string): Sort => {
  const [sortKey, sortValue] = sort.split(":");

  return {
    [sortKey]: sortMap[sortValue],
  };
};
