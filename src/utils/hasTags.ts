import { TagSchema } from "../entities/BaseEstate/models/BaseEstateModel";

export const hasTags = (tags: TagSchema[], tagsToHave: string[]): boolean =>
  tags.some((tag) => tagsToHave.includes(tag.name));
