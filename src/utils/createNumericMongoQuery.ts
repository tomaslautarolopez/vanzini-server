const comparatorMap: { [key: string]: string } = {
  ">=": "$gte",
  "=>": "$gte",
  "=": "$eq",
  "<=": "$lte",
  "=<": "$lte",
  ">": "$gt",
  "<": "$lt",
  "!=": "$ne",
};

const comparatorMapToFun: {
  [key: string]: (a: number) => (b: number) => boolean;
} = {
  ">=": (a: number) => (b: number) => a <= b,
  "=>": (a: number) => (b: number) => a <= b,
  "=": (a: number) => (b: number) => a === b,
  "<=": (a: number) => (b: number) => a >= b,
  "=<": (a: number) => (b: number) => a >= b,
  ">": (a: number) => (b: number) => a < b,
  "<": (a: number) => (b: number) => a > b,
  "!=": (a: number) => (b: number) => a !== b,
};

export const createNumericMongoQuery = (query: string) => {
  const mongoQuery: { [key: string]: number } = {};

  query.split(",").forEach((comparisson: string) => {
    const [stringNumber, comparator] = comparisson.split(":");
    const number = Number(stringNumber);

    mongoQuery[comparatorMap[comparator]] = number;
  });

  return mongoQuery;
};

export const createNumericEvaluator = (query: string): ((b: number) => boolean) => {
  const [stringNumber, comparator] = query.split(":");
  const number = Number(stringNumber);

  return comparatorMapToFun[comparator](number);
};
