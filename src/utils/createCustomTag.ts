import { CustomTagsInfoSchema } from "../entities/BaseEstate/models/BaseEstateModel";

export const createCustomTag = (customTags: any): string[] => {
  return customTags.reduce(
    (acum: string[], actTag: CustomTagsInfoSchema): string[] => (
      actTag.group_name === "Forma De Pago" && acum.push(actTag.public_name),
      acum
    ),
    []
  );
};
