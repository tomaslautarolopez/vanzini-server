export interface Projection {
  [key: string]: 1 | { $meta: "textScore" };
}

export const createProjectionObject = (fields: string[]): Projection =>
  fields.reduce((projObj: { [key: string]: 1 }, project: string) => {
    const field = project.split("|")[0];
    const value = project.split("|")[1];
    return {
      ...projObj,
      [field]: value ? JSON.parse(value) : 1,
    };
  }, {});
