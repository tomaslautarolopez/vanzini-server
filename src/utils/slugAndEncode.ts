import slugify from "slugify";

export const slugAndEncode = (text: string): string => {
  return encodeURIComponent(
    slugify(text, {
      replacement: "-",
      remove: /[*+~.()'"!:@\/]/g,
      lower: true,
    })
  );
};
