import { pickBy, isUndefined } from "lodash";

export function cleanUndefinedValuesInObject(object: Object) {
  return pickBy(object, (fil) => !isUndefined(fil));
}
