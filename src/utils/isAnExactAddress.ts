export const isAnExactAddress = (address: string) => {
  const splitedBySpace = address.split(" ");

  return (
    !isNaN(Number(splitedBySpace[splitedBySpace.length - 1])) ||
    address.includes(" y ")
  );
};
