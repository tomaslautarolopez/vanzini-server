import express from "express";
import helmet from "helmet";
import http from "http";
import bodyParser from "body-parser";
import morgan from "morgan";
import passport from "passport";
import { Server } from "socket.io";

import { mainApiRouter } from "./routes";
import { initMongoConnection } from "./store";
import { port, node_env } from "./config/config";
import { errorHandlerMiddleware } from "./errorHandling/errorHandlerMiddleware";
import { socketIOinit } from "./socket";

const debug = require("debug")("server");

const app = express();

const httpServer = http.createServer(app);
export const ioSocket = new Server(httpServer, {
  cors: {
    origin: "*",
  },
  path: "/sockets",
});
socketIOinit();

app.use(morgan("dev"));
app.use(bodyParser.urlencoded());
app.use(bodyParser.json({ limit: "20mb" }));
app.use(helmet());
app.use(passport.initialize());

// API Endpoints
app.use("/api", mainApiRouter);

app.use(errorHandlerMiddleware);

// Default Endpoint
app.get("/", async (req: any, res: any) => {
  res.send("LucioXd");
});

initMongoConnection(app);

app.on(
  "ready",
  async (): Promise<void> => {
    httpServer.listen(port, (): void => {
      debug(`App is on ${node_env} mode`);
      debug(`App is running on http://localhost:${port}`);
    });
  }
);
