import { Employee } from "./src/entities/Employee/Employee";

declare global {
  interface Mail {
    sendMailWithFrom: () => void;
  }
  namespace Express {
    interface Request {
      employee: Employee;
    }
  }
}
