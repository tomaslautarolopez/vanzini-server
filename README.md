# Sentinel Service

## How to run the app 🖥️

This is very easy 🤣, just install dependecies and run the server.

YOU MUST HAVE AN ENVIRONMENT VARIABLES FILE IN THE FOLDER `./src/config`

```
yarn dev
```

## Available Scripts

In the project directory, you can run:

### `yarn generate`

This comando will create a template for a new end-point, middeware or cron job. I strongly recommend using it to avoid namming issues and losing time.

### `yarn dev`

Runs the app in the development mode.<br />
The app is served in [http://localhost:3000](http://localhost:3000).

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
